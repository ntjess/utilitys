typing_extensions; python_version < '3.8'
pyqtgraph>=0.12.3
tqdm
numpy
pandas
docstring_parser
ruamel.yaml