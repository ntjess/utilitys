# Utilitys
## General Python Utilities

A collection of PyQt gui builders and other functions I've found helpful as I develop python mockups.

## Installation
`utilitys` lives in pypi too, so the easiest way to install it is through `pip`:
```bash
pip install utilitys
```
Note that a version of Qt must be installed for it to work, such as PyQt5/6 or Pyside2/6.
PyQt5 will also be installed with `pip install utilitys[full]`

Alternatively, you can do so through downloading the repository:
```bash
git clone https://gitlab.com/ntjess/utilitys
pip install -e ./utilitys # Or ./utilitys[full] as mentioned above
```

## Usage
Usage will vary depending on the requested capabilities. Check out the `examples` folder for some usages